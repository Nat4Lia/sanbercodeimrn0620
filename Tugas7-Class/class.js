// Soal No.1
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name() {
        return this._name;
    }

    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
}

var sheep = new Animal("shaun");
console.log("Class Animal");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log();

class Ape extends Animal {
    constructor(name) {
        super(name);
        this._legs = 2;
    }

    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
    }

    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
console.log("Class Ape Extends Animal");
sungokong.yell(); // "Auooo"
console.log(sungokong.name); // kera sakti
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
console.log();

var kodok = new Frog("buduk");
console.log("Class Frog Extends Animal");
kodok.jump(); // "Auooo"
console.log(kodok.name); // kera sakti
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // false
console.log();

// Soal No.2
class Clock {
    constructor( { template } ) {
        this.template = template;
        this.timer;
    }

    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
