var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];

var callReadbook = function(index, waktu) {
    if (index < books.length) {
        readBooksPromise(waktu, books[index])
            .then(function(sisaWaktu) {
                callReadbook(index + 1, sisaWaktu);
            })
            .catch(function(sisaWaktu){
                console.log(`waktu tidak cukup, kurang : ${-sisaWaktu}`);
            })
    }
}

callReadbook(0, 10000);