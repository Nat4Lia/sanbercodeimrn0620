// di index.js
var readBooks = require('./callback.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];

var callReadbook = function(index, waktu) {
    if (index < books.length) {
        readBooks(waktu, books[index], function(sisaWaktu) {
            callReadbook(index + 1, sisaWaktu);
        });
    } else {
        console.log('Tidak ada Buku lagi');
    }
};
callReadbook(0, 10000);