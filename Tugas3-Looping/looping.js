//Soal No.1
var deret = 2;
console.log('LOOPING PERTAMA');
while (deret <= 20) {
    if (deret % 2 == 0) {
        console.log(deret + ' - I Love Coding');  
    }
    deret++;
}

console.log('LOOPING KEDUA');
while (deret > 1) {
    if (deret % 2 == 0) {
        console.log(deret + ' - I will become a mobile developer');
    }
    deret--;
}
console.log();

//Soal No.2
for (var index = 1; index < 21 ; index++) {
    if (index % 2 == 1 && index % 3 == 0) {
        console.log(index+' - I Love Coding ');
    } else if (index%2==0 ) {
        console.log(index+' - Berkualitas');
    } else if (index%2==1) {
        console.log(index+' - Santai');
    }    
}
console.log();

//Soal No.3
for (var baris = 0; baris < 4; baris++) {
    var pagar = '';
    for (var kolom = 0; kolom < 8; kolom++) {
        pagar += '#';
    }
    console.log(pagar);
}
console.log();

//Soal No.4
for (var baris = 1; baris < 8; baris++) {
    var pagar = '';
    for (var kolom = 0; kolom < baris; kolom++) {
        pagar += '#';
    }
    console.log(pagar);
}
console.log();

//Soal No.5
for (var baris = 0; baris < 8; baris++) {
    var pagar = '';
    for (var kolom = 0; kolom < 8; kolom++) {
        // penentuan pengisian kolom
        if ( (kolom + baris) % 2 == 0){
            pagar += '#';
        } else {
            pagar += ' ';
        }
    }
    console.log(pagar);
}