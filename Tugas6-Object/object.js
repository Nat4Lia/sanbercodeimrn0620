// Soal No.1
function arrayToObject(paramsArray) {
    if (paramsArray.length == 0) {
        console.log('""');
    } else {
        var now = new Date();
        var thisYear = now.getFullYear();
        var result = {};
        for (var index = 0; index < paramsArray.length; index++) {
            var fullName = paramsArray[index][0] + " " + paramsArray[index][1];
            result[index + 1 + ". " + fullName] = {
                firstName: paramsArray[index][0],
                lastName: paramsArray[index][1],
                gender: paramsArray[index][2],
                age: function(thisYear, birthYear) {
                    if (birthYear == undefined || birthYear > thisYear) {
                        return "Invalid birth year";
                    } else {
                        return thisYear - birthYear;
                    }
                }(thisYear, paramsArray[index][3])
            };
        }
        console.log(result);
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people);
arrayToObject(people2);
arrayToObject([]);
console.log();

// Soal No.2
function shoppingTime(memberId, money) {
    sale = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000]
    ];
    if (memberId == undefined || memberId == ""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        if (money < 50000){
            return "Mohon maaf, uang tidak cukup";
        } else {
            var result = {
                memberId: memberId,
                money: money,
                listPurchased: [],
                changeMoney: 0
            };
            for (var index = 0; index < sale.length; index++) {
                if (money >= sale[index][1]){
                    result.listPurchased.push(sale[index][0]);
                    money -= sale[index][1];
                }
            }
            result.changeMoney = money;
            return result;
        }
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log();

// Soal No.3
function naikAngkot(listPenumpang) {
    if (listPenumpang.length == 0){
        return listPenumpang;
    } else {
        var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
        var result = [];
        var harga = 2000;
        for (var penumpang = 0; penumpang < listPenumpang.length; penumpang++) {
            var start, end = 0;
            for (var index = 0; index < rute.length; index++) {
                if (listPenumpang[penumpang][1] == rute[index])  {
                    start = index;
                }
                if (listPenumpang[penumpang][2] == rute[index]) {
                    end = index;
                }
            }
            result.push({
                penumpang: listPenumpang[penumpang][0],
                naikDari: listPenumpang[penumpang][1],
                tujuan: listPenumpang[penumpang][2],
                bayar: harga * rute.slice(start, end).length
            });      
        }
        return result;
    }
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));
