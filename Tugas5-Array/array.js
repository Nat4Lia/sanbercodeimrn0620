// Soal No.1
function range(startNum, finishNum) {
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else {
        var rangeArray = [];
        for (
                var index = startNum;
                (startNum < finishNum) ? index <= finishNum : index >= finishNum; 
                (startNum < finishNum) ? index++ : index--
            ) {
            rangeArray.push(index);
        }
        return rangeArray;
    }
}

console.log(range(70, 50));
console.log();

// Soal No.2
function rangeWithStep(startNum, finishNum, step) {
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else {
        var rangeArray = [];
        for (
                var index = startNum;
                (startNum < finishNum) ? index <= finishNum : index >= finishNum; 
                (startNum < finishNum) ? index += step : index -= step
            ) {
            rangeArray.push(index);
        }
        return rangeArray;
    }
}

console.log(rangeWithStep(29, 2, 4));
console.log();

// Soal No.3
function sum(startNum, finishNum, step) {
    step ? step : step = 1;    
    if (startNum != undefined && finishNum == undefined) {
        return 1;
    } else if (startNum == undefined) {
        return 0;
    } else {
        var result = 0;
        for (
                var index = startNum;
                (startNum < finishNum) ? index <= finishNum : index >= finishNum; 
                (startNum < finishNum) ? index += step : index -= step
            ) {
            result += index;            
        }
        return result;
    }
}

console.log(sum());
console.log();

// Soal No.4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(n) {
    for (var index = 0; index < n.length; index++) {
        console.log(
            "Nomor ID: "+n[index][0]+"\n"+
            "Nama Lengkap: "+n[index][1]+"\n"+
            "TTL: "+n[index][2]+" "+n[index][3]+"\n"+
            "Hobi: "+n[index][4]+"\n"
        );
    }
}

// dataHandling(input);

// Soal No.5
function balikKata(text) {
    var reverseText = "";    
    for (var index = text.length-1; index >= 0; index--) {
        reverseText += text[index];
    }
    return reverseText;
}

console.log(balikKata("I am Sanbers"));
console.log();

// Soal No.6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]
function monthToText(monthNumber) {
    switch (monthNumber) {
        case "01":
            monthNumber = "Januari";
            break;
        
        case "02":
            monthNumber = "Februari";
            break;

        case "03":
            monthNumber = "Maret";
            break;

        case "04":
            monthNumber = "April";
            break;

        case "05":
            monthNumber = "Mei";
            break;
        
        case "06":
            monthNumber = "Juni";
            break;

        case "07":
            monthNumber = "Juli";
            break;

        case "08":
            monthNumber = "Agustus";
            break;
        
        case "09":
            monthNumber = "September";
            break;
        
        case "10":
            monthNumber = "Oktober";
            break;

        case "12":
            monthNumber = "November";
            break;

        case "12":
            monthNumber = "Desember";
            break;
        default:
            break;
    }
    return monthNumber;
}
function dataHandling2(n) {
    var result = []
    n.splice(
        1,
        4,
        "Roman Alamsyah Elsharawy",
        "Provinsi Bandar Lampung",
        "21/05/1989",
        "Pria",
        "SMA Internasional Metro"
    );
    result[0] = n;
    result[1] = monthToText(n[3].split("/")[1]);
    result[2] = n[3].split("/").sort(
                    function compareNumbers(a, b) {
                        a = parseInt(a);
                        b = parseInt(b);
                        return a - b;
                    }
                ).reverse();
    result[3] = n[3].split("/").join("-");
    result[4] = n[1].split(" ").slice(0, 2).join(" ");
    return result;
}

console.log(dataHandling2(input));
