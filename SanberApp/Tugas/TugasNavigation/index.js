import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IconEntypo from 'react-native-vector-icons/Entypo';

import { Login } from './Login';
import { About } from './About';
import { SkillScreen } from './SkillScreen';
import { Project } from './ProjectScreen';
import { Add } from './AddScreen';

const LoginStack = createStackNavigator();
const AboutStack = createStackNavigator();
const SkillStack = createStackNavigator();

const LoginStackScreen = () => (
    <LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={Login}/>
    </LoginStack.Navigator>
)

const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen 
        name="About" 
        component={About}
        options={{
            title: '',
            headerStyle: {
                backgroundColor: '#F9BE7C',
                elevation: 0
            }
        }}
    />
    </AboutStack.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
    <Drawer.Navigator
        drawerContentOptions={{
            style: {
                backgroundColor: '#FFF9EC'
            },
            activeTintColor: '#EA820B',
            inactiveTintColor: '#0D253F',
            labelStyle: {
                fontFamily: 'Poppins'
            }
        }}
    >
        <Drawer.Screen name="About" component={AboutStackScreen}/>
        <Drawer.Screen name="Skill" component={TabScreen}/>
    </Drawer.Navigator>
)

const Tabs = createBottomTabNavigator();
const TabScreen = () => (
    <Tabs.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;
                if (route.name === "SkillScreen"){
                    iconName = "light-bulb"
                } else if (route.name === "Project"){
                    iconName = "open-book"
                } else if (route.name === "Add"){
                    iconName = "squared-plus"
                }
                return <IconEntypo name={iconName} size={size} color={color} />
            }
        })}
        tabBarOptions={{
            style: {
                backgroundColor: "#F9BE7C",
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                height: 60,
            },
            inactiveTintColor: '#0D253F',
            activeTintColor: '#FFF9EC',
            labelStyle: {
                fontFamily: 'Poppins-Medium',
            }
        }}
    >
        <Tabs.Screen name="SkillScreen" component={SkillScreen} />
        <Tabs.Screen name="Project" component={Project}/>
        <Tabs.Screen name="Add" component={Add}/>
    </Tabs.Navigator>
)

export default Index = () => (
    <NavigationContainer>
        <LoginStack.Navigator screenOptions={{
            headerShown: false
        }}>
            <LoginStack.Screen name="Login" component={Login}/>
            <LoginStack.Screen name="Drawer" component={DrawerScreen}/>
        </LoginStack.Navigator>
    </NavigationContainer>
)