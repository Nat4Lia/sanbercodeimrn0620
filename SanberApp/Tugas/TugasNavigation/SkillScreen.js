import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';

export const SkillScreen = ({ navigation }) => (
  <View style={styles.container}>
    <View style={styles.header}>
      <Text style={styles.headerText}>Programming Skill</Text>
    </View>

    <ScrollView style={styles.isi}>
      <View style={[styles.skillCont, {backgroundColor: '#F6CBCF'}]}>
        <View style={styles.skillHeader}>
          <Text style={styles.skillHeaderText}>Bahasa Pemrograman</Text>
        </View>

        <View style={styles.skillItem}>
          <IconMCI name="language-javascript" size={40} color='#0D253F' style={{marginRight: 3}}/>
          <View style={styles.itemTextCont}>
            <Text style={[styles.itemText,{fontSize: 16}]}>Intermediate Javascript</Text>
            <Text style={[styles.itemText,{fontSize: 12, marginTop: -8}]}>Penguasaan 60%</Text>
          </View>
        </View>

      </View>
      <View style={[styles.skillCont, {backgroundColor: '#D0EFF0'}]}>
        <View style={styles.skillHeader}>
          <Text style={styles.skillHeaderText}>Framework/Library</Text>
        </View>

        <View style={styles.skillItem}>
          <IconMCI name="react" size={40} color='#0D253F' style={{marginRight: 3}}/>
          <View style={styles.itemTextCont}>
            <Text style={[styles.itemText,{fontSize: 16}]}>Basic React Native</Text>
            <Text style={[styles.itemText,{fontSize: 12, marginTop: -8}]}>Penguasaan 60%</Text>
          </View>
        </View>

        <View style={styles.skillItem}>
          <IconMCI name="nodejs" size={40} color='#0D253F' style={{marginRight: 3}}/>
          <View style={styles.itemTextCont}>
            <Text style={[styles.itemText,{fontSize: 16}]}>Basic NodeJS</Text>
            <Text style={[styles.itemText,{fontSize: 12, marginTop: -8}]}>Penguasaan 60%</Text>
          </View>
        </View>
      </View>
      <View style={[styles.skillCont, {backgroundColor: '#FCE2C5'}]}>
        <View style={styles.skillHeader}>
          <Text style={styles.skillHeaderText}>Teknologi</Text>
        </View>

        <View style={styles.skillItem}>
          <IconMCI name="github-circle" size={40} color='#0D253F' style={{marginRight: 3}}/>
          <View style={styles.itemTextCont}>
            <Text style={[styles.itemText,{fontSize: 16}]}>Intermediate Github</Text>
            <Text style={[styles.itemText,{fontSize: 12, marginTop: -8}]}>Penguasaan 60%</Text>
          </View>
        </View>

        <View style={styles.skillItem}>
          <IconMCI name="gitlab" size={40} color='#0D253F' style={{marginRight: 3}}/>
          <View style={styles.itemTextCont}>
            <Text style={[styles.itemText,{fontSize: 16}]}>Intermediate Gitlab</Text>
            <Text style={[styles.itemText,{fontSize: 12, marginTop: -8}]}>Penguasaan 60%</Text>
          </View>
        </View>

        <View style={styles.skillItem}>
          <IconMCI name="visual-studio" size={40} color='#0D253F' style={{marginRight: 3}}/>
          <View style={styles.itemTextCont}>
            <Text style={[styles.itemText,{fontSize: 16}]}>Intermediate Visual Studio</Text>
            <Text style={[styles.itemText,{fontSize: 12, marginTop: -8}]}>Penguasaan 60%</Text>
          </View>
        </View>
      </View>
    </ScrollView>

    {/* <View style={styles.menu}>
      <TouchableOpacity>
        <Icon
          name="home"
          size={48}
          style={styles.iconMenu}
        />
      </TouchableOpacity>
      <TouchableOpacity>
        <Icon
          name="account-circle"
          size={48}
          style={styles.iconMenu}
        />
      </TouchableOpacity>
    </View> */}

  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF9EC'
  },
  header: {
    backgroundColor: '#F9BE7C',
    height: 60,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  headerText: {
    fontSize: 24,
    fontFamily: 'Poppins-Bold',
    color: '#0D253F'
  },
  menu: {
    backgroundColor: '#F9BE7C',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 60
  },
  isi: {
    flex: 2,
    paddingHorizontal: 10,
    marginVertical: 10
  },
  skillCont: {
    borderRadius: 10,
    marginBottom: 15,
    padding: 10
  },
  skillHeader: {
    borderBottomWidth: 1,
    color: '#0D253F',
    marginBottom: 5
  },
  skillHeaderText: {
    fontSize: 18,
    fontFamily: 'Poppins-Bold',
    color: '#0D253F'
  },
  skillItem: {
    flexDirection: 'row',
    marginBottom: 3
  },
  itemTextCont: {
    margin: 0,
  },
  itemText: {
    fontFamily: 'Poppins-Medium',
    color: '#0D253F',
    paddingBottom: 0
  }
})