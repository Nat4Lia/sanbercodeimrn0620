/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Logo from '../Tugas13/assets/components/logo';
import InputTextIcon from '../Tugas13/assets/components/inputtexticon';

export const Login = ({ navigation }) => (
  <KeyboardAvoidingView style={styles.container} behavior="height">
      <View style={{marginTop: 20}}>
        <Logo/>
      </View>

      <View style={{
        alignItems: "center",
        marginVertical: 20,
      }}>
        <InputTextIcon 
          iconName={"account-circle"} 
          label={"Username"}
          style={{marginVertical: 100}}
        />
        <InputTextIcon 
          iconName={"lock"} 
          label={"Password"}
        />
        <TouchableOpacity 
          style={styles.buttonStyle}
          activeOpacity={.8}
          onPress = {() => 
            navigation.navigate('Drawer', {
              screen: 'DrawerScreen'
            })
          }
        >
          <Text 
            style={{
              fontFamily: "Poppins-SemiBold",
              color: "#FFF9EC",
              alignSelf: "center",
              fontSize: 16
            }}
          >Login</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF9EC",
    justifyContent: 'center',
  },
  buttonStyle: {
    elevation: 4,
    backgroundColor: "#F9BE7C",
    width: 280,
    height: 36,
    borderRadius: 25,
    paddingVertical: 5,
    marginVertical: 15
  }
})