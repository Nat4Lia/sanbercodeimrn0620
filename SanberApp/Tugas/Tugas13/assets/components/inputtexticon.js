import React, { Component } from 'react';
import {
    View,
    TextInput,
    StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class InputTextIcon extends Component {
    render() {
        let iconName = this.props.iconName;
        let label = this.props.label;
        return(
            <View style={styles.container}>
                <Icon 
                    name={iconName} 
                    size={32}
                    style={{
                        paddingTop: 10,
                        paddingRight: 3
                    }}
                />
                <TextInput 
                    placeholder={label}
                    placeholderTextColor="#0D253F"
                    style={styles.textInput}
                    selectionColor="#309397"
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    textInput: {
        fontSize: 16,
        fontFamily: "Poppins-Regular",
        color: "#0D253F",
        width: 248,
        borderBottomColor: "#0D253F",
        borderBottomWidth: .9,
        paddingBottom: 0,
    }

})