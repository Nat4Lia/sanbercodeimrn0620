import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';

export default class Logo extends Component {
    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.sanberText}>SANBER</Text>
                <View style={styles.appTextCont}>
                    <View style={styles.line}></View>
                    <Text style={styles.applicationText}>APPLICATION</Text>
                    <View style={styles.line}></View>
                </View>
                <Image 
                    source={require('../images/Logo.png')} 
                    style={styles.imgLogo}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    sanberText: {
        fontFamily: "ONEDAY",
        fontSize: 48
    },
    appTextCont: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    applicationText: {
        fontFamily: "ONEDAY",
        fontSize: 14
    },
    line: {
        width: 42,
        borderBottomWidth: 1,
        borderColor: "#0D253F"
    },
    imgLogo: {
        width: 207, height: 207
    }

})