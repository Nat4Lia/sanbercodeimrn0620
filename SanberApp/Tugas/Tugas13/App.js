/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import Logo from './assets/components/logo';
import InputTextIcon from './assets/components/inputtexticon';

const App: () => React$Node = () => {

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{
          borderColor: "#F9BE7C",
          borderRadius: 100,
          borderWidth: 10,
          height: 118,
          width: 118,
          borderRadius: 59,
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: -30
        }}>
          <View
            style={{
              width: 118,
              height: 118,
              position: 'absolute',
              borderWidth: 10,
              borderTopColor: 'transparent',
              borderBottomColor: "#E46472",
              borderRightColor: "#E46472",
              borderLeftColor: "transparent",
              borderRadius: 100,
              transform: [{rotate: "-45deg"}]
            }}
          />
          <Image 
            source={require('./assets/images/Foto-Profile.jpg')}
            style={{
              width: 95,
              height: 95,
              borderRadius: 100
            }}
          />
        </View>
        
        <View 
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10
        }}>
          <Text style={styles.name}>Cahya Wening</Text>
          <Text style={styles.job}>Software Developer</Text>
        </View>
      </View>

      <View style={styles.containerIsi}>
        <View
          style={{
            marginVertical: 10,
            paddingHorizontal: 20
          }}
        >
          <Text style={styles.judul}>About</Text>
          <Text style={{
            fontFamily: "Poppins-Light",
            fontSize: 12
          }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus nibh diam nec id. Tempor mauris enim pellentesque praesent venenatis proin. Nibh vitae neque, eu cum turpis ullamcorper. Nunc ultrices volutpat porta quis id consequat, sagittis scelerisque. Libero velit quis felis morbi.
          </Text>
        </View>

        <View
          style={{
            marginVertical: 10,
            paddingHorizontal: 20
          }}
        >
          <Text style={styles.judul}>Social Media</Text>
          <View style={styles.containerSosmed}>
            <IconEntypo 
              name="twitter-with-circle"
              size={24}
              style={{marginRight: 5, color: "#00ACEE"}}
            />
            <Text style={styles.textSosmed}>@cahya_7</Text>
          </View>
          <View style={styles.containerSosmed}>
            <IconEntypo 
              name="facebook-with-circle"
              size={24}
              style={{marginRight: 5, color: "#3B5998"}}
            />
            <Text style={styles.textSosmed}>cahya.wening.7</Text>
          </View>
          <View style={styles.containerSosmed}>
            <IconEntypo 
              name="instagram-with-circle"
              size={24}
              style={{marginRight: 5, color: "#E1306C"}}
            />
            <Text style={styles.textSosmed}>@cahya.7</Text>
          </View>
        </View>

        <View
          style={{
            marginVertical: 10,
            paddingHorizontal: 20
          }}
        >
          <Text style={styles.judul}>Portofolio</Text>
          <Text 
            style={{
              fontFamily: "Poppins-Regular",
              fontSize: 16
            }}
          > · gitlab.com/Nat4Lia/sanbercodeimrn0620</Text>
        </View>
      </View>

      <View style={styles.menu}>
        <TouchableOpacity>
          <Icon
            name="home"
            size={48}
            style={styles.iconMenu}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Icon
            name="account-circle"
            size={48}
            style={styles.iconMenu}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF9EC",
    justifyContent: 'space-between'
  },
  header: {
    backgroundColor: "#F9BE7C",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    height: 142,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  name: {
    fontFamily: "Poppins-Bold",
    fontSize: 16,
    color: "#0D253F",
    marginBottom: -10
  },
  job:{
    fontFamily: "Poppins-Light",
    fontSize: 16,
    color: "#0D253F"
  },
  menu: {
    backgroundColor: "#F9BE7C",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  iconMenu: {
    padding: 7,
    color: "#0D253F"
  },
  containerIsi: {
    top: -35
  },
  judul: {
    fontFamily: "Poppins-Medium",
    fontSize: 16,
    marginBottom: 5
  },
  containerSosmed: {
    flexDirection: 'row',
    marginVertical: 2
  },
  textSosmed: {
    fontFamily: "Poppins-Regular",
    fontSize: 16
  }
})

export default App;
